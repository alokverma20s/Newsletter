const express = require("express");
const bodyParser = require("body-parser");
const https = require("https");
const client = require("@mailchimp/mailchimp_marketing");

// client.setConfig({
//     apiKey: "efdd89ce45c01c4bbdc96f7f8dcc4435-us17",
//     server: "us17"
// });
// const run = async () => {
//     const response = await client.root.getList("59421cf65f79aa8452e58bb22");
//     console.log(response);
// };

// run()

const app = express();
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", function (req, res) {
    res.sendFile(__dirname + "/signup.html");
});

app.post("/", function (req, res) {
    var firstName = req.body.FirstName;
    var lastName = req.body.LastName;
    var email = req.body.email;
    var data = {
        members: [
            {
                email_address : email,
                status: "subscribed",
                merge_field: {
                    FNAME: firstName,
                    LNAME: lastName
                }
            }
        ]
    }
    var jsonData = JSON.stringify(data);
    console.log(jsonData);
    var url = "https://us17.api.mailchimp.com/3.0/lists/877c617f8e"

    const options ={
        method : "POST",
        auth: "alok1:efdd89ce45c01c4bbdc96f7f8dcc4435-us17"
    }
    const request = https.request(url, options, function(response){
        if(response.statusCode === 200){
            res.sendFile(__dirname+"/success.html");
        }
        else{
            res.sendFile(__dirname+"/failure.html")
        }
    });
    request.write(jsonData);
    request.end();
});

app.post("/failure", function(req, res){
    res.redirect("/");
})

app.listen(process.env.PORT||3000, function () {
    console.log("Server is running on port 3000");
});



// API KEY
// efdd89ce45c01c4bbdc96f7f8dcc4435-us17

// Audience ID
// 877c617f8e

// const mailchimp = require("@mailchimp/mailchimp_marketing");

// mailchimp.setConfig({
//     apiKey: "efdd89ce45c01c4bbdc96f7f8dcc4435-us17",
//     server: "us17",
// });

// async function run() {
//     const response = await mailchimp.ping.get();
//     console.log(response);
// }
// run();
